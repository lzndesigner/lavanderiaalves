@extends('frontend.base')
@section('content')
<section class="careers pb-70 bg-gray">
      <div class="container">
      <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="heading heading-layout3 mb-40">
              <h3 class="heading__title">Lavanderia na Mídia</h3>
              <h2 class="heading__subtitle">A Lavanderia Alves possui uma equipe pronta para atender empresas, atendimento de chamadas domésticas e públicos em geral. Acompanhe as novidades:</h2>
            </div><!-- /.heading -->
          </div><!-- /.col-lg-10 -->
        </div><!-- /.row -->
        <div class="row mb-50">
          <div class="col-xs-12 col-md-6">
            <div class="job-item">
              <div class="iframe-container">
                <iframe src="https://www.youtube.com/embed/fV9UL0XnJvQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-md-6">
            <div class="job-item">
              <div class="iframe-container">
              <iframe src="https://www.youtube.com/embed/2jH3HtJehkA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
          </div>
        </div><!-- /.row -->

        <div class="row mb-50">
          <div class="col-xs-12 col-md-6">
            <div class="job-item">
              <div class="iframe-container">
                <iframe src="https://www.youtube.com/embed/kOdasixpjmk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-md-6">
            <div class="job-item">
              <div class="iframe-container">
              <iframe src="https://www.youtube.com/embed/hG_qUocfmtA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
          </div>
        </div><!-- /.row -->

        <div class="row mb-50">
          <div class="col-xs-12 col-md-6">
            <div class="job-item">
              <div class="iframe-container">
                <iframe src="https://www.youtube.com/embed/2Y1ioUK2SRI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
          </div>
        </div><!-- /.row -->


      </div><!-- /.container -->
    </section>
@endsection