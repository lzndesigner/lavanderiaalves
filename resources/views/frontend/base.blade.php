<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <base href="{{ Request::url() }}" />
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Lavanderia Alves | Lavagem de Sofás, carpetes, Cortinas, Tapetes, Persianas e Impermeabilização de estofados
    </title>
    <meta name="description" content="Atendemos toda São Paulo e grande São Paulo, não cobramos por retirada e entrega. Temos mais de 32 anos de tradição. Clique e venha conhecer nossa lavanderia. Melhor preço de São Paulo.">
    <meta name="robots" content="index, follow" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Lavanderia Alves | Atendemos toda São Paulo e grande São Paulo, não cobramos por retirada e entrega. Temos mais de 32 anos de tradição. Clique e venha conhecer nossa lavanderia. Melhor preço de São Paulo." />
    <meta property="og:description" content="Atendemos toda São Paulo e grande São Paulo, não cobramos por entrega e temos mais de 32 anos de tradição. Clique e venha conhecer nossa lavanderia. Melhor preço de São Paulo." />
    <meta property="og:url" content="https://lavanderiaalves.com.br/" />
    <meta property="og:site_name" content="Lavanderia Alves" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Atendemos toda São Paulo e grande São Paulo, não cobramos por retirada e entrega. Temos mais de 32 anos de tradição. Clique e venha conhecer nossa lavanderia. Melhor preço de São Paulo." />
    <meta name="twitter:title" content="Lavanderia Alves | Atendemos toda São Paulo e grande São Paulo, não cobramos por retirada e entrega. Temos mais de 32 anos de tradição. Clique e venha conhecer nossa lavanderia. Melhor preço de São Paulo." />
    <link rel="icon" href="{{ asset('/galerias/favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('/minify/css/all.css?3') }}">


    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-5NZ6KRL');
    </script>
    <!-- End Google Tag Manager -->

</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5NZ6KRL" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="wrapper">
        <!-- =========================
        Header
    =========================== -->
        <header id="header" class="header header-light header-layout1">
            <div class="header-topbar d-none d-xl-block">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 col-md-7 col-lg-7">
                            <div class="d-flex flex-wrap justify-content-between">
                                <ul class="contact-list list-unstyled mb-0 d-flex flex-wrap">
                                    <li>
                                        <i class="fa fa-phone"></i>
                                        <a href="tel:551156225931">(11) 5622-5931</a>
                                    </li>
                                    <li><i class="fa fa-clock"></i><span>Horário: Seg-Sex: 8am – 7pm</span> - Sab: 8am -
                                        14pm</li>
                                </ul>
                            </div>
                        </div><!-- /.col-lg-7 -->
                        <div class="col-sm-12 col-md-5 col-lg-5 d-flex flex-wrap justify-content-end">
                            <ul class="social-icons list-unstyled mb-0">
                                <li><a href="https://api.whatsapp.com/send?phone=5511954920850&text=Ola%20gostaria%20de%20fazer%20um%20or%C3%A7amento%20de%20cortina.%20Te%20encontrei%20nos%20site%20Header.%20Muito%20obrigado" target="_Blank"><i class="fab fa-whatsapp"></i> Faça seu orçamento - 11
                                        9.5492-0850</a></li>
                            </ul>
                        </div><!-- /.col-lg-5 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div>
            <nav class="navbar navbar-expand-lg">
                <div class="container-fluid px-0">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ asset('/galerias/logo.png?1') }}" class="logo-dark" alt="Lavanderia Alves" width="180" height="100%">
                    </a>
                    <button class="navbar-toggler" type="button">
                        <span class="menu-lines"><span></span></span>
                    </button>
                    <div class="collapse navbar-collapse" id="mainNavigation">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav__item ">
                                <a href="{{ url('/') }}" class="nav__item-link @if (collect(request()->segments())->last() == '') active @endif">Início</a>
                            </li><!-- /.nav-item -->
                            <li class="nav__item">
                                <a href="{{ url('/servicos') }}" class="nav__item-link @if (collect(request()->segments())->last() == 'servicos' ||
                                    collect(request()->segments())->last() == 'lavagem-estofados' ||
                                    collect(request()->segments())->last() == 'lavagem-tapetes' ||
                                    collect(request()->segments())->last() == 'lavagem-cortinas' ||
                                    collect(request()->segments())->last() == 'lavagem-persianas' ||
                                    collect(request()->segments())->last() == 'lavagem-carpetes' ||
                                    collect(request()->segments())->last() == 'impermeabilizacao') active @endif">Serviços</a>
                            </li><!-- /.nav-item -->
                            <li class="nav__item">
                                <a href="{{ url('/namidia') }}" class="nav__item-link @if (collect(request()->segments())->last() == 'namidia') active @endif">Na Mídia</a>
                            </li><!-- /.nav-item -->
                            <li class="nav__item">
                                <a href="https://api.whatsapp.com/send?phone=5511954920850&text=Ola%20gostaria%20de%20fazer%20um%20or%C3%A7amento%20de%20cortina.%20Te%20encontrei%20nos%20site%20Header.%20Muito%20obrigado" target="_Blank" class="nav__item-link @if (collect(request()->segments())->last() == 'orcamento') active @endif">Orçamento</a>
                            </li><!-- /.nav-item -->
                            <li class="nav__item">
                                @if (collect(request()->segments())->last() == '' || collect(request()->segments())->last() == 'orcamento')
                                <a href="#" id="locallink" class="nav__item-link btn-scroll">Local</a>
                                @else
                                <a href="{{ url('/local') }}" class="nav__item-link @if (collect(request()->segments())->last() == 'local') active @endif">Local</a>
                                @endif
                            </li><!-- /.nav-item -->
                        </ul><!-- /.navbar-nav -->
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container -->
            </nav><!-- /.navabr -->
        </header><!-- /.Header -->

        @yield('content')

        <!-- ========================
      Footer
    ========================== -->
        <footer class="footer">
            <div class="footer-primary">
                <div class="container">
                    <div class="row d-flex justify-content-between">
                        <div class="col-sm-12 col-md-12 col-lg-7 col-xl-7">
                            <div class="footer-widget footer-widget-about pr-0">
                                <h6 class="footer-widget-title">Sobre nós</h6>
                                <div class="footer-widget-content">
                                    <p class="mb-20">Desde 1984 a Lavanderia Alves proporciona a seus clientes um
                                        serviço de extrema qualidade na lavagem de sofás, tapetes, carpetes, cortinas,
                                        persianas, alem de impermeabilização e restauração de tapetes. <br> CNPJ:
                                        36.262.435/0001-51</p>
                                </div>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="footer-widget footer-widget-contact">
                                            <h6 class="footer-widget-title">Nossos Serviços</h6>
                                            <ul class="list-unstyled">
                                                <li><a href="{{ url('/lavagem-de-estofados') }}">Lavagem Estofados</a></li>
                                                <li><a href="{{ url('/lavagem-de-tapetes') }}">Lavagem Tapetes</a></li>
                                                <li><a href="{{ url('/lavagem-de-cortinas') }}">Lavagem Cortinas</a></li>
                                                <li><a href="{{ url('/lavagem-de-persianas') }}">Lavagem Persianas</a></li>
                                                <li><a href="{{ url('/lavagem-de-carpetes') }}">Lavagem Carpetes</a></li>
                                                <li><a href="{{ url('/impermeabilizacao') }}">Impermeabilizacao</a></li>
                                            </ul>
                                        </div>
                                        <div class="footer-widget-content">
                                            <p class="mt-3"><b>Tecnologia:</b> <br> <a href="https://jpwtechdigital.com.br" target="_Blank"><img src="{{ asset('/galerias/jpw.webp?2') }}" alt="JPW Tech Digital" width="120" height="100%" style="max-width:110px;"></a> </p>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="col-xs-12">
                                            <div class="footer-widget footer-widget-contact">
                                                <h6 class="footer-widget-title">Seja um Parceiro</h6>
                                                <p>Conheça as vantagens de ser um Parceiro da Lavanderia Alves entre em contato
                                                    para maiores informações.</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="footer-widget footer-widget-contact">
                                                <h6 class="footer-widget-title">Seja um Cliente VIP</h6>
                                                <p>Conheça as vantagens de ser um Cliente VIP da Lavanderia Alves, entre em
                                                    contato para maiores informações.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col-xl-4 -->
                        <div class="col-sm-12 col-md-12 col-lg-5 col-xl-5">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                                    <div class="footer-widget footer-widget-contact">
                                        <h6 class="footer-widget-title">Entre em Contato</h6>
                                        <div class="footer-widget-content">
                                            <p class="mb-20">Será um prazer atende-lo!</p>
                                            <a class="contact-number contact-number-white d-flex justify-content-center mb-20" href="tel:551156225931">
                                                <i class="fa fa-phone"></i><span>(11) 5622-5931</span>
                                            </a><!-- /.contact__numbr -->
                                            <a class="contact-number contact-number-white d-flex justify-content-center mb-20" href="tel:5511954920850">
                                                <i class="fa fa-phone"></i><span>(11) 9.5492-0850</span>
                                            </a><!-- /.contact__numbr -->
                                            <p class="mb-30"><b>Lavanderia Alves - SEDE</b> <br> R. Baquirivu, 454 <br> Cidade Ademar - São Paulo - Cep: 04404-030</p>

                                            <p class="mb-30"><b>Lavanderia Alves - Jd. Prudência</b> <br> Avenida Cupecê, 1029 <br> Jardim Prudência - São Paulo - Cep: 04365-000</p>

                                            <p class="mb-30"><b>Lavanderia Alves - Vila Mascote</b> <br> Avenida Damasceno Vieira, 997 <br> Vila Mascote - São Paulo - Cep: 04363-040</p>

                                            <p class="mb-30"><b>Lavanderia Alves - Cursino</b> <br> Avenida do Cursino, 61 <br> Vila da Saúde - São Paulo - Cep: 04133-000</p>

                                            <p class="mb-30"><b>Lavanderia Alves - Vila Guarani</b> <br> Avenida Dr. Hugo Beolchi, 751 <br> Vila Guarani - São Paulo - Cep: 04310-030</p>
                                        </div><!-- /.footer-widget-content -->
                                    </div>
                                </div>
                            </div><!-- row -->

                        </div><!-- /.col-xl-4 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.footer-primary -->
            <div class="footer-copyrights">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                            <p class="mb-0"><a href="/">Lavanderia Alves</a> - Todos os direitos reservados 1984 ~ 2020
                                &copy;</p>
                        </div><!-- /.col-lg-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.footer-copyrights-->
        </footer><!-- /.Footer -->
        <button id="scrollTopBtn"><i class="fas fa-long-arrow-alt-up"></i></button>

        <a href="https://api.whatsapp.com/send?phone=5511954920850&text=Ola%20gostaria%20de%20fazer%20um%20or%C3%A7amento%20de%20cortina.%20Te%20encontrei%20nos%20site%20Header.%20Muito%20obrigado" class="btn-whatsapp" target="_Blank"><i class="fab fa-whatsapp"></i></a>

    </div><!-- /.wrapper -->

    <script src="{{ asset('/frontend/js/jquery-3.5.1.min.js?2') }}"></script>
    <script src="https://kit.fontawesome.com/04571ab3d2.js" crossorigin="anonymous"></script>
    <script src="{{ asset('/frontend/js/plugins.js?2') }}"></script>
    <script src="{{ asset('/frontend/js/main.js?3') }}"></script>
    <script src="{{ asset('/frontend/js/jquery.mask.js?2') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            // $('input[name="postcode"]').mask('00000-000');
            // $("input[name='postcode']").mask('00000-000', {reverse: true, placeholder: "00000-000"});
            // $('input[name="custom_field[account][1]"]').mask('000.000.000-00', {reverse: true});
            $('input[name="phone"]').mask("(99) 99999-9999");
            $('input[name="phone"]').on("blur", function() {
                var last = $(this).val().substr($(this).val().indexOf("-") + 1);

                if (last.length == 3) {
                    var move = $(this).val().substr($(this).val().indexOf("-") - 1, 1);
                    var lastfour = move + last;
                    var first = $(this).val().substr(0, 9);

                    $(this).val(first + '-' + lastfour);
                }
            });
        });
    </script>
    <script>
        // This is a functions that scrolls to #{blah}link
        function goToByScroll(id) {
            // Remove "link" from the ID
            id = id.replace("link", "");
            // Scroll
            $('html,body').animate({
                scrollTop: $("#" + id).offset().top
            }, 'slow');
        }

        $(".btn-scroll").click(function(e) {
            // Prevent a page reload when a link is pressed
            e.preventDefault();
            // Call the scroll function
            goToByScroll(this.id);
        });

        $('.gotoOrcamento').click(function(e) {
            e.preventDefault();
            goToByScroll('orcamentolink');
        });
    </script>
</body>

</html>