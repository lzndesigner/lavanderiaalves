<section class="contact-layout2 py-20 bg-gray">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="contact-panel">
          <div class="testimonials testimonials-layout1 bg-overlay bg-overlay-theme">
            <div class="slick-carousel" data-slick='{"slidesToShow": 1, "arrows": true, "dots": false, "infinite": true}'>
              <!-- Testimonial #1 -->
              <div class="testimonial-item">
                <div class="testimonial-item__rating">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
                <p class="testimonial-item__desc">Desde 1984 a Lavanderia Alves proporciona a seus clientes um serviço de extrema qualidade na lavagem de sofás, tapetes, carpetes, cortinas, persianas, alem de impermeabilização e restauração de tapetes.
                </p>
                <div class="testimonial-item__meta d-none">
                  <div class="testimonial-item__thumb">
                    <img src="{{ asset('/galerias/avatares/1.png') }}" alt="Martin Hope">
                  </div><!-- /.testimonial-thumb -->
                  <div>
                    <h4 class="testimonial-item__meta-title">Martin Hope</h4>
                    <p class="testimonial-item__meta-desc">Pro Cons</p>
                  </div>
                </div><!-- /.testimonial-meta -->
              </div><!-- /. testimonial-item -->

            </div>
          </div><!-- /.testimonials-layout1 -->
          <div class="contact-panel__form">
            <form method="post" action="{{ route('orcar.sendmail') }}" id="contactForm">
              @csrf
              <div class="row">
                <div class="col-sm-12">
                  <h4 class="contact-panel__title">Realize seu Orçamento</h4>
                  <p class="contact-panel__desc mb-40">Olá. Preencha os campos abaixo para realizarmos seu orçamento. Entraremos em contato através do seu e-mail, telefone ou whatsapp.
                  </p>
                </div>
                <div class="col-sm-12 col-md-5 col-lg-5">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Seu nome" id="name" name="name" required>
                  </div>
                </div><!-- /.col-lg-6 -->
                <div class="col-sm-12 col-md-7 col-lg-7">
                  <div class="form-group">
                    <input type="email" class="form-control" placeholder="Seu E-mail" id="email" name="email" required>
                  </div>
                </div><!-- /.col-lg-6 -->
                <div class="col-sm-12 col-md-3 col-lg-4">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Seu telefone" id="phone" name="phone" required>
                  </div>
                </div><!-- /.col-lg-6 -->
                <div class="col-sm-12 col-md-4 col-lg-4">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Seu bairro" id="distric" name="distric" required>
                  </div>
                </div><!-- /.col-lg-6 -->
                <div class="col-sm-12 col-md-4 col-lg-4">
                  <div class="form-group">
                    <select id="referencia" name="referencia" class="form-control" style="display: none;">
                      <option>Como nos conheceu?</option>
                      <option>Encontrei no Google</option>
                      <option>Folhetos</option>
                      <option>Indicação de Amigos</option>
                      <option>Outra forma...</option>
                    </select>
                  </div>
                </div><!-- /.col-lg-6 -->
                <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <textarea class="form-control" placeholder="Digite sua mensagem" id="message" name="message" required></textarea>
                  </div>
                </div><!-- /.col-lg-12 -->
                <div class="col-sm-12 col-md-12 col-lg-12 d-flex flex-wrap align-items-center">
                  <button type="submit" class="btn btn__secondary mr-40">
                    <i class="fa fa-arrow-right"></i> <span>Enviar Orçamento</span>
                  </button>
                  <div class="form-group input-radio my-3">
                    <span>Prazo estimado de resposta é de 1 dia</span>
                  </div>
                </div><!-- /.col-lg-12 -->
              </div>
            </form>
            <div class="contact-result"></div>
          </div>
        </div><!-- /.contact__panel -->
      </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
  </div><!-- /.container -->
</section><!-- /.contact layout 2 -->