@extends('frontend.base')
@section('content')
<section class="portfolio-single pt-0 pb-50 page-services">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="portfolio-item">
          <div class="portfolio-item__img">
            <img src="{{ asset('/galerias/paginas/titulo-lavagem-de-carpetes-1.jpg') }}" alt="Lavagem de Carpetes" class="img-fluid">
          </div>
          <div class="portfolio-item__content text-center pt-5">
            <h1 class="portfolio-item__title">Lavagem de Carpetes</h1>
            <p class="">A Lavanderia Alves é uma empresa com mais de 32 anos de experiência que executa serviços de lavagem de carpetes. Contamos com uma excelente estrutura para atender todas as demandas por lavagem de carpetes com muita eficiência.</p>
            <p class="">Oferecemos as melhores soluções para lavagem de carpetes, seja em sua residência ou empresa. Trabalhamos com o objetivo de atender todas as expectativas dos clientes que solicitam serviço de lavagem de carpetes.</p>
          </div><!-- /.portfolio-content -->
        </div>
      </div>
    </div><!-- row -->


    <div class="row mb-50">
      <div class="col-sm-12 col-md-12 col-lg-6">
        <div class="text-block">
          <h5 class="text-block__title mb-30">Conheça os tipos</h5>
          <div class="text-block__content">
            <p class="text-block__desc">Extração de alta performance da sujeira e resíduos.</p>
            <p class="text-block__desc">Tratamento para lavagem de manchas, especiais como manchas de urina animal no carpete.</p>
            <p class="text-block__desc">A remoção total das manchas depender de fatores como estado do carpete, tempo da mancha, tipo e não é garantida.</p>
          </div>
        </div><!-- /.text-block -->
      </div><!-- /.col-lg-6 -->
      <div class="col-sm-12 col-md-12 col-lg-6">
        <h5 class="text-block__title mb-30">Garantia de Qualidade:</h5>
        <p class="text-block__desc">Todo o processo é feito pela própria Lavanderia Alves, sem terceirização. Profissionais especializados e altamente treinados.</p>
        <p class="text-block__desc">Equipamentos modernos e produtos para eliminação de ácaros e bactérias e especifico para uma perfeita higienização</p>
      </div><!-- /.col-lg-5 -->
    </div><!-- row -->

    
    <div class="row mt-50">
      <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="text-block">
          <h5 class="text-block__title mb-30">Você não precisa deixar o local para a Lavanderia Alves lavar seu carpete.</h5>
          <div class="text-block__content">
            <p class="text-block__desc">Parceria com fabricantes para constante inovação de produtos e técnicas de lavagem. Perfeita higienização com produtos específicos e máquinas de enxágue com extração a vácuo, agilizando a secagem do carpetes</p>
          </div>
        </div><!-- /.text-block -->
      </div><!-- /.col-lg-6 -->
    </div><!-- row -->
  </div><!-- container -->
</section>

@include('frontend.orcamento_form')
@endsection