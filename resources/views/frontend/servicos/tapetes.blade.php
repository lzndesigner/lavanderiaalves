@extends('frontend.base')
@section('content')
<section class="portfolio-single pt-0 pb-50 page-services">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="portfolio-item">
          <div class="portfolio-item__img">
            <img src="{{ asset('/galerias/paginas/lavanderia-de-tapetes.jpg') }}" alt="Lavagem de Tapetes" class="img-fluid">
          </div>
          <div class="portfolio-item__content text-center pt-5">
            <h1 class="portfolio-item__title">Lavagem de Tapetes</h1>
            <p class="">Com mais de 30 anos de experiência, a Lavanderia Alves executa os serviços de lavagem de tapetes, independente do material e tecido confeccionado.</p>
            <p class="">Além de todo o profissionalismo, você conta com a lavanderia de melhor estrutura, capaz de suprir as demandas de clientes físicos ou jurídicos, com extrema eficiência.</p>
            <p class="">Nosso objetivo é sempre superar as expectativas dos nossos clientes.</p>
          </div><!-- /.portfolio-content -->
        </div>
      </div>
    </div><!-- row -->


    <div class="row mb-50">
      <div class="col-sm-12 col-md-12 col-lg-6">
        <div class="text-block">
          <h5 class="text-block__title mb-30">Conheça os tipos</h5>
          <div class="text-block__content">
            <p class="text-block__desc">Lavamos Tapetes: Chinês; Iraniano; Kilim; Marroquino; Persa. Tratando todos como verdadeiras obras de arte.</p>
            <p class="text-block__desc">• Capacidade para lavar e higienizar tapetes de grandes proporções.</p>
            <p class="text-block__desc">• Trabalhamos com tratamento específico para a lavagem de manchas.</p>
            <p class="text-block__desc">• Lavagem de tapetes com marcas específicas, principalmente, de urina animal.</p>
            <p class="text-block__desc">• A remoção total das manchas não é garantida, dependendo de fatores como o estado do tapete, espécie de mancha, tempo de mancha, etc.</p>
          </div>
        </div><!-- /.text-block -->
      </div><!-- /.col-lg-6 -->
      <div class="col-sm-12 col-md-12 col-lg-6">
        <div class="text-block">
          <h5 class="text-block__title mb-30">Acompanhe no Youtube</h5>
          <div class="iframe-container">
            <iframe src="https://www.youtube.com/embed/2xm4VDNMM2s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div><!-- videoresponsive -->
        </div><!-- videoresponsive -->
      </div><!-- /.col-lg-5 -->
    </div><!-- row -->

    <div class="row">
      <div class="col-sm-12 col-md-12 mb-20">
        <div class="text-block">
          <h5 class="text-block__title mb-30">Garantia de Qualidade</h5>
          <div class="text-block__content">
            <p class="text-block__desc">A Lavanderia Alves é especializada na limpeza de tapetes, nacionais e importados, independente do material e tecido. Retiramos e retornamos os tapetes já higienizados, sem custos adicionais, em até 5 dias úteis — sempre com dia e hora marcada, para a sua maior comodidade.</p>
            <p class="text-block__desc">Todo o processo de limpeza é feito por profissionais da casa, sem terceirizados, capacitados para identificar cada necessidade e processo específico a ser realizado.
              Com a lavagem a seco ou com água, a Lavanderia Alves ainda conta com centrífugas de até 100kg de capacidade, para uma secagem mais rápida e uniforme, responsável pela remoção total de resíduos sem danificar o tecido.</p>
          </div>
        </div>
      </div>
    </div>

    <div class="row mb-20">
      <div class="col-sm-12 col-md-12 col-lg-12 mb-20">
        <div class="text-block">
          <h5 class="text-block__title mb-30">Serviços adicionais:</h5>
          <div class="text-block__content">
            <p class="text-block__desc">Também realizamos a colocação de franjas, cordões e forros antiderrapantes.</p>
          </div>
        </div><!-- /.text-block -->
      </div><!-- /.col-lg-6 -->
      <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="text-block">
          <h5 class="text-block__title mb-30">Processo de Lavagem</h5>
          <div class="text-block__content">
            <p class="text-block__desc">Nosso método de lavagem é realizado a partir da junção da tecnologia e trabalho manual, para obter o máximo de resultado. <br> Também efetuamos parceria com os fabricantes para otimizar nossas técnicas de lavagem e inovar em produtos.</p>
            <p class="text-block__desc">Higienização completa e perfeita dos tapetes a partir de máquinas e produtos específicos, com enxágue com extração à vácuo. <br> Secagem do tapete em estufa exclusiva, com temperatura controlada para cada tipo de fibra e tamanho, evitando assim riscos de ressecamento ou alteração de cor.</p>
          </div>
        </div><!-- /.text-block -->
      </div><!-- /.col-lg-6 -->
    </div><!-- row -->
  </div><!-- container -->
</section>

@include('frontend.orcamento_form')
@endsection