@extends('frontend.base')
@section('content')
<section class="portfolio-single pt-0 pb-50 page-services">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="portfolio-item">
          <div class="portfolio-item__img">
            <img src="{{ asset('/galerias/paginas/titulo-lavagem-de-cortina.jpg') }}" alt="Lavagem de Cortinas" class="img-fluid">
          </div>
          <div class="portfolio-item__content text-center pt-5">
            <h1 class="portfolio-item__title">Lavagem de Cortinas</h1>
            <p class="">Com mais de 30 anos de experiência, a Lavanderia Alves executa os serviços de lavagem de cortinas.</p>
            <p class="">Além de todo o profissionalismo, você conta com a lavanderia de melhor estrutura, capaz de suprir as demandas de clientes físicos ou jurídicos, com extrema eficiência.</p>
            <p class="">Nosso objetivo é sempre superar as expectativas dos nossos clientes.</p>
          </div><!-- /.portfolio-content -->
        </div>
      </div>
    </div><!-- row -->


    <div class="row mb-50">
      <div class="col-sm-12 col-md-12 col-lg-7">
        <div class="text-block">
          <h5 class="text-block__title mb-30">Lavagem de cortinas e tecidos sofisticados</h5>
          <div class="text-block__content">
            <p class="text-block__desc">• Lavamos Cortinas: de seda; de linho; voil; shantung; chales; bandôs; painéis; cordões; pingentes. Entregues limpas e minuciosamente passadas.</p>
            <p class="text-block__desc">• Nossas costureiras refazem as barras, caso seja necessário.</p>
            <p class="text-block__desc">• Capacidade para lavar e higienizar cortinas de tamanhos grandes.</p>
            <p class="text-block__desc">• Trabalhamos com profissionais treinados e capacitados.</p>
            <p class="text-block__desc">Retiramos e entregamos os produtos limpos e higienizados sem custo adicional, em até 5 dias úteis.</p>
          </div>
        </div><!-- /.text-block -->
      </div><!-- /.col-lg-6 -->
      <div class="col-sm-12 col-md-12 col-lg-5">
        <h5 class="text-block__title mb-30">Garantia de Qualidade</h5>
        <p class="text-block__desc">A Lavanderia Alves é especializada na lavagem de cortinas. Todo o processo de limpeza é feito por profissionais da casa, sem terceirizados, capacitados para identificar cada necessidade e processo específico a ser realizado.</p>
        <p class="text-block__desc">Retiramos e retornamos as cortinas já higienizadas, sem custos adicionais, em até 5 dias úteis – sempre com dia e hora marcada, para a sua maior comodidade.</p>
      </div><!-- /.col-lg-5 -->
    </div><!-- row -->
  </div><!-- container -->
</section>

@include('frontend.orcamento_form')
@endsection