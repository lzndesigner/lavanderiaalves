@extends('frontend.base')
@section('content')
<section class="portfolio-single pt-0 pb-50 page-services">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="portfolio-item">
          <div class="portfolio-item__img">
            <img src="{{ asset('/galerias/paginas/titulo-lavagem-de-estofados.jpg') }}" alt="Lavagem de Estofados" class="img-fluid">
          </div>
          <div class="portfolio-item__content text-center pt-5">
            <h1 class="portfolio-item__title">Lavagem de Sofás e Estofados</h1>
            <p class="">Com mais de 30 anos de experiência, a Lavanderia Alves executa os serviços de lavagem de estofados e sofás na sua residência ou empresa.</p>
            <p class="">Além da comodidade, você conta com a lavanderia de melhor estrutura, capaz de suprir as demandas de clientes físicos ou jurídicos, com muita eficiência.</p>
            <p class="">Nosso objetivo é sempre superar as expectativas dos nossos clientes.</p>
          </div><!-- /.portfolio-content -->
        </div>
      </div>
    </div><!-- row -->


    <div class="row mb-50">
      <div class="col-sm-12 col-md-12 col-lg-6">
        <div class="text-block">
          <h5 class="text-block__title mb-30">Conheça os tipos</h5>
          <div class="text-block__content">
            <p class="text-block__desc">• Lavamos: Almofadas; Bancos; Cadeiras/ Cadeiras de Escritório; Colchões; Futons; Poltronas; Sofás. Obtendo o máximo de resultado.</p>
            <p class="text-block__desc">• Trabalhamos com tratamento específico para a lavagem de manchas.</p>
            <p class="text-block__desc">• Lavagem de estofados com marcas de urina, café, caneta, leite, doces, outros.</p>
            <p class="text-block__desc">• A remoção total das manchas não é garantida, dependendo de fatores como o estado do estofado, espécie de mancha, tempo de mancha, etc.</p>
          </div>
        </div><!-- /.text-block -->
      </div><!-- /.col-lg-6 -->
      <div class="col-sm-12 col-md-12 col-lg-6">
        <div class="text-block">
          <h5 class="text-block__title mb-30">Acompanhe no Youtube</h5>
          <div class="iframe-container">
            <iframe src="https://www.youtube.com/embed/6_GySh4m1js" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
      </div><!-- /.col-lg-5 -->
    </div><!-- row -->

    <div class="row">
      <div class="col-sm-12 col-md-12 mb-20">
        <div class="text-block">
          <h5 class="text-block__title mb-30">Garantia de Qualidade</h5>
          <p class="text-block__desc">
            • A Lavanderia Alves é especializada na lavagem de sofás e estofados, e para isso conta com profissionais qualificados, treinados e capacitados.
          </p>
          <p class="text-block__desc">
            • Todo o processo de limpeza é feito no próprio local do cliente, seja residencial ou comercial, sem a intermediação de terceirizados.
          </p>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12 mb-20">
        <div class="text-block">
          <h5 class="text-block__title mb-30">Serviços adicionais:</h5>
          <div class="text-block__content">
            <p class="text-block__desc">Recomendamos e realizamos a impermeabilização do estofado para a manutenção da limpeza, evitando assim manchas em caso de queda de líquidos sobre o tecido (como sucos, refrigerantes, produtos químicos, etc).</p>
          </div>
        </div><!-- /.text-block -->
      </div><!-- /.col-lg-6 -->
      <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="text-block">
          <h5 class="text-block__title mb-30">Processo de Lavagem de Sofás e Estofados:</h5>
          <div class="text-block__content">
            <p class="text-block__desc">Nosso método de higienização e limpeza de sofás e estofados é realizado a partir da junção da tecnologia e trabalho manual, para obter o máximo de resultado.</p>
            <p class="text-block__desc">Também efetuamos parceria com os fabricantes para otimizar nossas técnicas de lavagem e inovar em produtos.
              Higienização completa e perfeita do estofado por meio da lavagem especial com água, escovas específicas e equipamentos modernos de aspiração, além de produtos específicos.</p>
          </div>
        </div><!-- /.text-block -->
      </div><!-- /.col-lg-6 -->
    </div><!-- row -->
  </div><!-- container -->
</section>

@include('frontend.orcamento_form')
@endsection