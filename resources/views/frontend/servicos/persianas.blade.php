@extends('frontend.base')
@section('content')
<section class="portfolio-single pt-0 pb-50 page-services">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="portfolio-item">
          <div class="portfolio-item__img">
            <img src="{{ asset('/galerias/paginas/lavagem-de-persianas-imgs-1.jpg') }}" alt="Lavagem de Persianas" class="img-fluid">
          </div>
          <div class="portfolio-item__content text-center pt-5">
            <h1 class="portfolio-item__title">Lavagem de Persianas</h1>
            <p class="">Com mais de 30 anos de experiência, a Lavanderia Alves executa os serviços de lavagem de persianas, além de manutenção, reinstalação, recortes, adaptação para tamanhos diferentes, reformas em geral e consertos.</p>
            <p class="">Além de todo o profissionalismo, você conta com a lavanderia de melhor estrutura, capaz de suprir as demandas de clientes físicos ou jurídicos, com extrema eficiência.</p>
            <p class="">Nosso objetivo é sempre superar as expectativas dos nossos clientes.</p>
          </div><!-- /.portfolio-content -->
        </div>
      </div>
    </div><!-- row -->


    <div class="row mb-50">
      <div class="col-sm-12 col-md-12 col-lg-6">
        <div class="text-block">
          <h5 class="text-block__title mb-30">Conheça os tipos</h5>
          <div class="text-block__content">
            <p class="text-block__desc">• Lavamos as persianas por meio de métodos especiais e produtos específicos, focados em não danificá-las no que se refere a pintura, textura, formato e impermeabilização.</p>
            <p class="text-block__desc">• Processo de secagem realizado 100% em ambiente natural, fundamental para garantir a qualidade do serviço.</p>
            <p class="text-block__desc">• Utilizamos a técnica adequada para cada tipo específico de persiana, eliminando toda a sujeira por igual.</p>
            <p class="text-block__desc">• Retiramos e entregamos os produtos limpos e higienizados sem custo adicional, em até 5 dias úteis.</p>
          </div>
        </div><!-- /.text-block -->
      </div><!-- /.col-lg-6 -->
      <div class="col-sm-12 col-md-12 col-lg-6">
        <h5 class="text-block__title mb-30">Lavagem <b>não retira</b>:</h5>
        <p class="text-block__desc">• Efeitos deixados pelo tempo (desgaste, cor amarelada ou manchas de poluição).</p>
        <p class="text-block__desc">• Graxa (parcialmente).</p>
        <p class="text-block__desc">• Ferrugem (parcialmente).</p>
      </div><!-- /.col-lg-5 -->
    </div><!-- row -->
  </div><!-- container -->
</section>

@include('frontend.orcamento_form')
@endsection