@extends('frontend.base')
@section('content')
<section class="portfolio-single pt-0 pb-50">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="portfolio-item">
          <div class="portfolio-item__content text-center pt-5">
            <h1 class="portfolio-item__title">Faça seu Orçamento - Limpeza de Sofá</h1>
          </div><!-- /.portfolio-content -->
        </div>
      </div>
    </div><!-- row -->

    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-8">
        <a href="https://api.whatsapp.com/send?phone=5511954920850&amp;text=Ola%20gostaria%20de%20fazer%20um%20or%C3%A7amento%20de%20sofa.%20Te%20encontrei%20no%20Gif%20de%20WhatsApp.%20Muito%20obrigado" class="no-lightbox">
          <img width="350" height="622" alt="" data-src="{{asset('/Gifs/sofa.gif')}}" class="attachment-large size-large lazyloaded" src="{{asset('/Gifs/sofa.gif')}}"><noscript><img width="350" height="622" src="{{asset('/Gifs/sofa.gif')}}" class="attachment-large size-large" alt="" /></noscript> </a>
      </div><!-- center -->
      <div class="col-sm-12 col-md-12 col-lg-4">
        <aside class="sidebar mb-30">

          <div class="widget widget-posts">
            <div class="widget-content">
              <div id="fb-root"></div>
              <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v8.0&appId=1094654680570525&autoLogAppEvents=1" nonce="DRtQaoW7"></script>
              <div class="fb-page" data-href="https://www.facebook.com/LAVANDERIAALVESSP/" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                <blockquote cite="https://www.facebook.com/LAVANDERIAALVESSP/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/LAVANDERIAALVESSP/">Lavanderia Alves</a></blockquote>
              </div>
            </div><!-- /.widget-content -->
          </div><!-- /.widget-posts -->
          <div class="widget widget-tags">
            <h5 class="widget-title">Sobre a Lavanderia Alves</h5>
            <div class="widget-content">
              <p>Desde 1984 a <b>Lavanderia Alves</b> proporciona a seus clientes um serviço de extrema qualidade na lavagem de sofás, tapetes, carpetes, cortinas, persianas, alem de impermeabilização e restauração de tapetes.</p>
            </div><!-- /.widget-content -->
          </div><!-- /.widget-tags -->
          <div class="widget widget-tags">
            <h5 class="widget-title">Links Rápidos</h5>
            <div class="widget-content">
              <ul class="list-unstyled">
                <li><a href="{{url('/')}}">Início</a></li>
                <li><a href="{{url('/orcamento')}}">Orçamento</a></li>
                <li><a href="{{url('/lavagem-de-estofados')}}">Lavagem Estofados</a></li>
                <li><a href="{{url('/lavagem-de-tapetes')}}">Lavagem Tapetes</a></li>
                <li><a href="{{url('/lavagem-de-cortinas')}}">Lavagem Cortinas</a></li>
                <li><a href="{{url('/lavagem-de-persianas')}}">Lavagem Persianas</a></li>
                <li><a href="{{url('/lavagem-de-carpetes')}}">Lavagem Carpetes</a></li>
                <li><a href="{{url('/impermeabilizacao')}}">Impermeabilizacao</a></li>
              </ul>
            </div><!-- /.widget-content -->
          </div><!-- /.widget-tags -->
        </aside><!-- /.sidebar -->
      </div><!-- aside -->
    </div><!-- row -->
  </div><!-- container -->
</section>
@endsection