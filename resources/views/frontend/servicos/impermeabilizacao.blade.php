@extends('frontend.base')
@section('content')
<section class="portfolio-single pt-0 pb-50 page-services">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="portfolio-item">
          <div class="portfolio-item__img">
            <img src="{{ asset('/galerias/paginas/titulo-impermeabilizacao.jpg') }}" alt="Impermeabilização de Estofados" class="img-fluid">
          </div>
          <div class="portfolio-item__content text-center pt-5">
            <h1 class="portfolio-item__title">Impermeabilização de Estofados</h1>
            <p class="">A Lavanderia Alves é uma empresa com mais de 32 anos de experiência que executa serviços de Impermeabilização de Estofados. Contamos com uma excelente estrutura para atender todas as demandas por impermeabilização com muita eficiência.</p>
            <p class="">Oferecemos as melhores soluções para impermeabilizar seu estofado, seja em sua residência ou empresa. Trabalhamos com o objetivo de atender todas as expectativas dos clientes que solicitam nossos serviços.</p>
          </div><!-- /.portfolio-content -->
        </div>
      </div>
    </div><!-- row -->


    <div class="row mb-50">
      <div class="col-sm-12 col-md-12 col-lg-6">
        <div class="text-block">
          <h5 class="text-block__title mb-30">O melhor produto do mercado</h5>
          <div class="text-block__content">
            <p class="text-block__desc">Os estofados são impermeabilizados pela Lavanderia Alves com ZONYL 2.000 da Dupont, Cappax da Cappa Comercial e Teximper água da Tex Imper Ltda, todos os produtos não inflamáveis, a base de água, que repelem líquidos, óleos, sujeiras secas e úmidas.</p>
            <p class="text-block__desc">Aplicação do Impermeabilizante não altera o tecido e nem mesmo a cor.
              O que ajuda a conservar ainda mais o tecido evitar manchas em caso de queda de líquidos como: água, refrigerantes, sucos e entre outros.</p>
          </div>
        </div><!-- /.text-block -->
      </div><!-- /.col-lg-6 -->
      <div class="col-sm-12 col-md-12 col-lg-6">
        <h5 class="text-block__title mb-30">Segurança e Qualidade:</h5>
        <p class="text-block__desc">Todo o processo de impermeabilização é feito pela própria Lavanderia Alves, sem terceirização.</p>
        <p class="text-block__desc">A Lavanderia Alves é especializada em impermeabilização.</p>
      </div><!-- /.col-lg-5 -->
    </div><!-- row -->


    <div class="row mt-50">
      <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="text-block">
          <h5 class="text-block__title mb-30">Praticidade e durabilidade na limpeza de seu estofado</h5>
          <div class="text-block__content">
            <p class="text-block__desc">O impermeabilizante de estofados é aplicado por pulverizador, pela Lavanderia Alves somente com tecido seco e limpo. Ele será um facilitador na manutenção da limpeza de estofados.</p>
          </div>
        </div><!-- /.text-block -->
      </div><!-- /.col-lg-6 -->
    </div><!-- row -->
  </div><!-- container -->
</section>

@include('frontend.orcamento_form')
@endsection