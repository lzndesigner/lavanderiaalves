@extends('frontend.base')
@section('content')
    <!-- ========================= 
         contact layout3
    =========================  -->
    <section class="contact-layout3 py-0" id="local">
      <div id="map" style="height: 720px;"></div>
      <script src="{{ asset('/frontend/js/google-map.js?1') }}"></script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhQ9BQLgbqh8lmDHBOKAFJ4NIbN95oAcA&callback=initMap" async defer></script>
      <!-- CLICK HERE (https://developers.google.com/maps/documentation/embed/get-api-key) TO  LERAN MORE ABOUT GOOGLE MAPS API KEY -->
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-4 offset-lg-8">
            <div class="contact-panel__info bg-white">
              <div class="contact-panel__block">
                <h5 class="contact-panel__block__title">Nossa Localização</h5>
                <ul class="contact-panel__block__list list-unstyled">
                  <li>R. Baquirivu, 454 - Cidade Ademar, São Paulo - SP</li>
                </ul>
              </div><!-- /.contact-panel__info__block -->
              <div class="contact-panel__block">
                <h5 class="contact-panel__block__title">Formas de Contato</h5>
                <ul class="contact-panel__block__list list-unstyled">
                  <li><a href="mailto:contato@lavanderiaalves.com.br"></a>E-mail: contato@lavanderiaalves.com.br</li>
                  <li><a href="tel:+551156218171"></a>Telefone: (11) 5621-8171</li>
                  <li><a href="tel:+551156225931"></a>Telefone: (11) 5622-5931</li>
                  <li><a href="tel:+5511954920850"></a>Telefone: (11) 9.5492-0850</li>
                </ul>
              </div><!-- /.contact-panel__info__block -->
              <div class="contact-panel__block">
                <h5 class="contact-panel__block__title">Horário de Funcionamento</h5>
                <ul class="contact-panel__block__list list-unstyled">
                  <li>Segunda ~ Sexta</li>
                  <li>08:00 AM às 19:00 PM</li>
                  <li></li>
                  <li>Sábado</li>
                  <li>08:00 AM às 14:00 PM</li>
                </ul>
              </div><!-- /.contact-panel__info__block -->
            </div>
          </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section><!-- /.contact-layout3 -->
@endsection