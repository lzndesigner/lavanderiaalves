@extends('frontend.base')
@section('content')
    <!-- ============================
                Slider
            ============================== -->
    <section class="slider slider-layout1">
        <div class="slick-carousel carousel-arrows-light m-slides-0"
            data-slick='{"slidesToShow": 1, "arrows": true, "dots": true, "speed": 700,"fade": true,"cssEase": "linear"}'>
            <div class="slide-item align-v-h bg-overlay">
                <div class="bg-img"><img src="{{ asset('/galerias/slider/sofas.webp?1') }}" alt="Estofados"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-9">
                            <div class="slide-item__content">
                                <h1 class="slide-item__title">Lavagem de Estofados</h1>
                                <p class="slide-item__desc">Lavagem feita no local, solicite sua cotação sem compromisso!
                                </p>
                                <a href="#" class="gotoOrcamento btn btn__primary btn__lg mr-30">
                                    <i class="fa fa-arrow-right"></i><span>Fale Conosco</span>
                                </a>
                            </div><!-- /.slide-content -->
                        </div><!-- /.col-xl-9 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.slide-item -->
            <div class="slide-item align-v-h bg-overlay">
                <div class="bg-img"><img src="{{ asset('/galerias/slider/persianas.webp?1') }}" alt="Persianas"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-9">
                            <div class="slide-item__content">
                                <h1 class="slide-item__title">Lavagem de Persianas</h1>
                                <p class="slide-item__desc">Conheça nossos serviços! Entrega e retirada gratuita!</p>
                                <a href="#" class="gotoOrcamento btn btn__primary btn__lg mr-30">
                                    <i class="fa fa-arrow-right"></i><span>Fale Conosco</span>
                                </a>
                            </div><!-- /.slide-content -->
                        </div><!-- /.col-xl-9 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.slide-item -->
            <div class="slide-item align-v-h bg-overlay">
                <div class="bg-img"><img src="{{ asset('/galerias/slider/cortinas.webp?2') }}" alt="Cortinas"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-9">
                            <div class="slide-item__content">
                                <h1 class="slide-item__title">Lavagem de Cortinas</h1>
                                <p class="slide-item__desc">Retirada e entrega Gratuita, tenha sua cortina higienizada em
                                    até 5 dias úteis.</p>
                                <a href="#" class="gotoOrcamento btn btn__primary btn__lg mr-30">
                                    <i class="fa fa-arrow-right"></i><span>Fale Conosco</span>
                                </a>
                            </div><!-- /.slide-content -->
                        </div><!-- /.col-xl-9 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.slide-item -->
            <div class="slide-item align-v-h bg-overlay">
                <div class="bg-img"><img src="{{ asset('/galerias/slider/tapetes.webp?2') }}" alt="Tapetes"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-9">
                            <div class="slide-item__content">
                                <h1 class="slide-item__title">Lavagem de Tapetes</h1>
                                <p class="slide-item__desc">Deixamos seus tapetes como novos!</p>
                                <a href="#" class="gotoOrcamento btn btn__primary btn__lg mr-30">
                                    <i class="fa fa-arrow-right"></i><span>Fale Conosco</span>
                                </a>
                            </div><!-- /.slide-content -->
                        </div><!-- /.col-xl-9 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.slide-item -->
        </div><!-- /.carousel -->
    </section><!-- /.slider -->

    <!-- ========================
            services
            =========================== -->
    <section class="py-5">
        <div class="container">
            <div class="heading-block center border-bottom-0 bottommargin-lg">
                <h1>Nossos Serviços</h1>
            </div>

            <div class="row mb-50">
                <div class="col-md-6">
                    <div class="feature-box feature-md media-box feature-marine">
                        <div class="fbox-media">
                            <img src="{{ asset('/galerias/paginas/lavagem-de-estofado-img.jpg') }}"
                                alt="Lavagem de Estofados" width="100%" height="100%">
                        </div>
                        <div class="fbox-content px-0">
                            <h3>Lavagem de Estofados</h3>
                            <p><b>Lavagem de Estofados:</b> limpeza completa com equipamentos e produtos modernos,
                                específicos. Eliminamos sujeiras e bactérias. Para a sua comodidade, a lavagem é feita no
                                próprio local.</p>
                            <a href="{{ url('/lavagem-de-estofados') }}"
                                class="button button-small button-dark button-rounded button-pink"><i
                                    class="fa fa-gift"></i>Saiba mais</a>
                            <a href="{{ url('/orcamento') }}" class="button button-small button-border button-rounded"><i
                                    class="fa fa-gift"></i>Orçamento</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="feature-box feature-md media-box flex-column feature-marine">
                        <div class="fbox-media">
                            <img src="{{ asset('/galerias/paginas/lavagem-de-tapetes-1.jpg') }}" alt="Lavagem de Tapetes" width="100%" height="100%">
                        </div>
                        <div class="fbox-content px-0">
                            <h3>Lavagem de Tapetes</h3>
                            <p><b>Lavagem de Tapetes:</b> retiramos e entregamos os produtos limpos e higienizados sem custo
                                adicional, em até 5 dias úteis. Todos os procedimentos são realizados com equipamentos
                                profissionais.</p>
                            <a href="{{ url('/lavagem-de-tapetes') }}"
                                class="button button-small button-dark button-rounded button-pink"><i
                                    class="fa fa-gift"></i>Saiba mais</a>
                            <a href="{{ url('/orcamento') }}" class="button button-small button-border button-rounded"><i
                                    class="fa fa-gift"></i>Orçamento</a>
                        </div>
                    </div>
                </div>

            </div><!-- row -->
            <div class="row mb-50">
                <div class="col-md-3">
                    <div class="feature-box feature-small media-box flex-column feature-blue">
                        <div class="fbox-media">
                            <img src="{{ asset('/galerias/paginas/lavagem-de-cortinas.jpg') }}"
                                alt="Lavagem de Cortinas" width="100%" height="100%">
                        </div>
                        <div class="fbox-content px-0">
                            <h3>Lavagem de Cortinas</h3>
                            <p><b>Lavagem de Cortinas:</b> retiramos e entregamos os produtos limpos e higienizados sem
                                custo adicional, em até 5 dias úteis. Todos os procedimentos são realizados com equipamentos
                                profissionais.</p>
                            <a href="{{ url('/lavagem-de-cortinas') }}"
                                class="button button-small button-dark button-rounded button-pink"><i
                                    class="fa fa-gift"></i>Saiba mais</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="feature-box feature-small media-box flex-column feature-blue">
                        <div class="fbox-media">
                            <img src="{{ asset('/galerias/paginas/lavagem-de-persianas-imgs.jpg') }}"
                                alt="Lavagem de Persianas" width="100%" height="100%">
                        </div>
                        <div class="fbox-content px-0">
                            <h3>Lavagem de Persianas</h3>
                            <p><b>Lavagem de Persianas:</b> retiramos e entregamos os produtos limpos e higienizados sem
                                custo adicional, em até 5 dias úteis. Todos os procedimentos são realizados com equipamentos
                                profissionais.</p>
                            <a href="{{ url('/lavagem-de-persianas') }}"
                                class="button button-small button-dark button-rounded button-pink"><i
                                    class="fa fa-gift"></i>Saiba mais</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="feature-box feature-small media-box flex-column feature-pink">
                        <div class="fbox-media">
                            <img src="{{ asset('/galerias/paginas/lavagem-de-carpetes.jpg') }}"
                                alt="Lavagem de Carpetes" width="100%" height="100%">
                        </div>
                        <div class="fbox-content px-0">
                            <h3>Lavagem de Carpetes</h3>
                            <p><b>Lavagem de Carpetes:</b> feito no próprio local com equipamentos profissionais, acabam com
                                toda sujeira, bactérias e fungos. Acesse nossa página e saiba como realizamos todo o
                                processo de limpeza.</p>
                            <a href="{{ url('/lavagem-de-carpetes') }}"
                                class="button button-small button-dark button-rounded button-pink"><i
                                    class="fa fa-gift"></i>Saiba mais</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="feature-box feature-small media-box flex-column feature-pink">
                        <div class="fbox-media">
                            <img src="{{ asset('/galerias/paginas/impermeabilizacao.webp') }}" alt="Impermeabilização" width="100%" height="100%">
                        </div>
                        <div class="fbox-content px-0">
                            <h3>Impermeabilização</h3>
                            <p><b>Impermeabilização:</b> sofás, cadeiras, estofados de carros entre outros, assim evitando
                                manchas no tecido e aumentando a durabilidade. Acesse nossa página e entenda mais sobre a
                                impermeabilização.</p>
                            <a href="{{ url('/impermeabilizacao') }}"
                                class="button button-small button-dark button-rounded button-pink"><i
                                    class="fa fa-gift"></i>Saiba mais</a>
                        </div>
                    </div>
                </div>

            </div><!-- row -->

        </div><!-- /.container -->
    </section><!-- /.features -->


    <section class="careers pb-70 bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="heading heading-layout3 mb-40">
                        <h3 class="heading__title">Lavanderia na Mídia</h3>
                        <h2 class="heading__subtitle">A Lavanderia Alves possui uma equipe pronta para atender empresas,
                            atendimento de chamadas domésticas e públicos em geral. Acompanhe as novidades:</h2>
                    </div><!-- /.heading -->
                </div><!-- /.col-lg-10 -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="job-item">
                        <a href="https://www.youtube.com/embed/fV9UL0XnJvQ" target="_Blank">
                            <img src="{{ asset('/galerias/videos/capa_1.webp') }}" alt="" class="img-responsive" width="100%" height="100%">
                            <p class="m-0">Assista no Youtube</p>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="job-item">
                        <a href="https://www.youtube.com/embed/2jH3HtJehkA" target="_Blank">
                            <img src="{{ asset('/galerias/videos/capa_2.webp') }}" alt="" class="img-responsive" width="100%" height="100%">
                            <p class="m-0">Assista no Youtube</p>
                        </a>
                    </div>
                    <div id="orcamento"></div>
                </div>
            </div><!-- /.row -->
            <a href="{{ url('/namidia') }}" class="button button-dark button-rounded button-pink mt-30"><i class="fa fa-gift"></i>
                <i class="fa fa-heart"></i> Veja mais Vídeos</a>
        </div><!-- /.container -->
    </section>
    <!-- ==========================
                contact layout 2
              =========================== -->
    <section class="contact-layout2 py-0 bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="contact-panel">
                        <div class="testimonials testimonials-layout1 bg-overlay bg-overlay-theme">
                            <div class="bg-img">
                                <img src="{{ asset('/galerias/backgrounds/1.webp') }}" alt="banner">
                            </div>
                            <div class="slick-carousel"
                                data-slick='{"slidesToShow": 1, "arrows": true, "dots": false, "infinite": true}'>
                                <!-- Testimonial #1 -->
                                <div class="testimonial-item">
                                    <div class="testimonial-item__rating">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <p class="testimonial-item__desc">Desde 1984 a Lavanderia Alves proporciona a seus
                                        clientes um serviço de extrema qualidade na lavagem de sofás, tapetes, carpetes,
                                        cortinas, persianas, alem de impermeabilização e restauração de tapetes.
                                    </p>
                                </div><!-- /. testimonial-item -->

                            </div>
                        </div><!-- /.testimonials-layout1 -->
                        <div class="contact-panel__form">
                            <form method="post" action="{{ route('orcar.sendmail') }}" id="contactForm">
                                @csrf
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4 class="contact-panel__title">Realize seu Orçamento</h4>
                                        <p class="contact-panel__desc mb-40">Olá. Preencha os campos abaixo para realizarmos
                                            seu orçamento. Entraremos em contato através do seu e-mail, telefone ou
                                            whatsapp.
                                        </p>
                                    </div>
                                    <div class="col-sm-12 col-md-5 col-lg-5">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Seu nome" id="name"
                                                name="name" required>
                                        </div>
                                    </div><!-- /.col-lg-6 -->
                                    <div class="col-sm-12 col-md-7 col-lg-7">
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Seu E-mail" id="email"
                                                name="email" required>
                                        </div>
                                    </div><!-- /.col-lg-6 -->
                                    <div class="col-sm-12 col-md-3 col-lg-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Seu telefone" id="phone"
                                                name="phone" required>
                                        </div>
                                    </div><!-- /.col-lg-6 -->
                                    <div class="col-sm-12 col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Seu bairro" id="distric"
                                                name="distric" required>
                                        </div>
                                    </div><!-- /.col-lg-6 -->
                                    <div class="col-sm-12 col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <select id="referencia" name="referencia" class="form-control"
                                                style="display: none;">
                                                <option>Como nos conheceu?</option>
                                                <option>Encontrei no Google</option>
                                                <option>Folhetos</option>
                                                <option>Indicação de Amigos</option>
                                                <option>Outra forma...</option>
                                            </select>
                                        </div>
                                    </div><!-- /.col-lg-6 -->
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <textarea class="form-control" placeholder="Digite sua mensagem" id="message"
                                                name="message" required></textarea>
                                        </div>
                                    </div><!-- /.col-lg-12 -->
                                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex flex-wrap align-items-center">
                                        <button type="submit" class="btn btn__secondary mr-40">
                                            <i class="fa fa-arrow-right"></i> <span>Enviar Orçamento</span>
                                        </button>
                                        <div class="form-group input-radio my-3">
                                            <span>Prazo estimado de resposta é de 1 dia</span>
                                        </div>
                                    </div><!-- /.col-lg-12 -->
                                </div>
                            </form>
                            <div class="contact-result"></div>
                        </div>
                    </div><!-- /.contact__panel -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.contact layout 2 -->

    <!-- ========================= 
                 contact layout3
            =========================  -->
    <section class="contact-layout3 py-0" id="local">
        <div id="map-img"
            onclick="window.open('https://www.google.com/maps/place/Lavanderia+Alves/@-23.6696218,-46.6595094,19z/data=!3m1!4b1!4m5!3m4!1s0x94ce45357a8ff599:0xba5a4506046e2368!8m2!3d-23.669623!4d-46.6589622?hl=pt-BR')">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-4 offset-lg-8">
                    <div class="contact-panel__info bg-white">
                        <div class="contact-panel__block">
                            <h5 class="contact-panel__block__title">Nossa Localização</h5>
                            <ul class="contact-panel__block__list list-unstyled">
                                <li>R. Baquirivu, 454 - Cidade Ademar, São Paulo - SP</li>
                            </ul>
                        </div><!-- /.contact-panel__info__block -->
                        <div class="contact-panel__block">
                            <h5 class="contact-panel__block__title">Formas de Contato</h5>
                            <ul class="contact-panel__block__list list-unstyled">
                                <li><a href="mailto:contato@lavanderiaalves.com.br"></a>E-mail:
                                    contato@lavanderiaalves.com.br</li>
                                <li><a href="tel:+551156225931"></a>Telefone: (11) 5622-5931</li>
                                <li><a href="tel:+5511954920850"></a>Telefone: (11) 9.5492-0850</li>
                            </ul>
                        </div><!-- /.contact-panel__info__block -->
                        <div class="contact-panel__block">
                            <h5 class="contact-panel__block__title">Horário de Funcionamento</h5>
                            <ul class="contact-panel__block__list list-unstyled">
                                <li>Segunda ~ Sexta</li>
                                <li>08:00 AM às 19:00 PM</li>
                                <li></li>
                                <li>Sábado</li>
                                <li>08:00 AM às 14:00 PM</li>
                            </ul>
                        </div><!-- /.contact-panel__info__block -->
                    </div>
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.contact-layout3 -->
@endsection
