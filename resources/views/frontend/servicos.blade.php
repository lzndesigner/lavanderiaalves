@extends('frontend.base')
@section('content')
    <!-- ========================
    services
    =========================== -->
    <section class="py-5">
      <div class="container">
        <div class="heading-block center border-bottom-0 bottommargin-lg">
          <h1>Nossos Serviços</h1>
        </div>

        <div class="row mb-50">
          <div class="col-md-6">
            <div class="feature-box feature-md media-box feature-marine">
              <div class="fbox-media">
                <img src="{{ asset('/galerias/paginas/lavagem-de-estofado-img.jpg') }}" alt="Lavagem de Estofados">
              </div>
              <div class="fbox-content px-0">
                <h3>Lavagem de Estofados</h3>
                <p><b>Lavagem de Estofados:</b> limpeza completa com equipamentos e produtos modernos, específicos. Eliminamos sujeiras e bactérias. Para a sua comodidade, a lavagem é feita no próprio local.</p>
                <a href="{{url('/lavagem-de-estofados')}}" class="button button-small button-dark button-rounded button-pink"><i class="fa fa-gift"></i>Saiba mais</a>
                <a href="{{url('/orcamento')}}" class="button button-small button-border button-rounded"><i class="fa fa-gift"></i>Orçamento</a>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="feature-box feature-md media-box flex-column feature-marine">
              <div class="fbox-media">
                <img src="{{ asset('/galerias/paginas/lavagem-de-tapetes-1.jpg') }}" alt="Lavagem de Tapetes">
              </div>
              <div class="fbox-content px-0">
                <h3>Lavagem de Tapetes</h3>
                <p><b>Lavagem de Tapetes:</b> retiramos e entregamos os produtos limpos e higienizados sem custo adicional, em até 5 dias úteis. Todos os procedimentos são realizados com equipamentos profissionais.</p>
                <a href="{{url('/lavagem-de-tapetes')}}" class="button button-small button-dark button-rounded button-pink"><i class="fa fa-gift"></i>Saiba mais</a>
                <a href="{{url('/orcamento')}}" class="button button-small button-border button-rounded"><i class="fa fa-gift"></i>Orçamento</a>
              </div>
            </div>
          </div>

        </div><!-- row -->
        <div class="row mb-50">
          <div class="col-md-3">
            <div class="feature-box feature-small media-box flex-column feature-blue">
              <div class="fbox-media">
                <img src="{{ asset('/galerias/paginas/lavagem-de-cortinas.jpg') }}" alt="Lavagem de Cortinas">
              </div>
              <div class="fbox-content px-0">
                <h3>Lavagem de Cortinas</h3>
                <p><b>Lavagem de Cortinas:</b> retiramos e entregamos os produtos limpos e higienizados sem custo adicional, em até 5 dias úteis. Todos os procedimentos são realizados com equipamentos profissionais.</p>
                <a href="{{url('/lavagem-de-cortinas')}}" class="button button-small button-dark button-rounded button-pink"><i class="fa fa-gift"></i>Saiba mais</a>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="feature-box feature-small media-box flex-column feature-blue">
              <div class="fbox-media">
                <img src="{{ asset('/galerias/paginas/lavagem-de-persianas-imgs.jpg') }}" alt="Lavagem de Persianas">
              </div>
              <div class="fbox-content px-0">
                <h3>Lavagem de Persianas</h3>
                <p><b>Lavagem de Persianas:</b> retiramos e entregamos os produtos limpos e higienizados sem custo adicional, em até 5 dias úteis. Todos os procedimentos são realizados com equipamentos profissionais.</p>
                <a href="{{url('/lavagem-de-persianas')}}" class="button button-small button-dark button-rounded button-pink"><i class="fa fa-gift"></i>Saiba mais</a>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="feature-box feature-small media-box flex-column feature-pink">
              <div class="fbox-media">
                <img src="{{ asset('/galerias/paginas/lavagem-de-carpetes.jpg') }}" alt="Lavagem de Carpetes">
              </div>
              <div class="fbox-content px-0">
                <h3>Lavagem de Carpetes</h3>
                <p><b>Lavagem de Carpetes:</b> feito no próprio local com equipamentos profissionais, acabam com toda sujeira, bactérias e fungos. Acesse nossa página e saiba como realizamos todo o processo de limpeza.</p>
                <a href="{{url('/lavagem-de-carpetes')}}" class="button button-small button-dark button-rounded button-pink"><i class="fa fa-gift"></i>Saiba mais</a>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="feature-box feature-small media-box flex-column feature-pink">
              <div class="fbox-media">
                <img src="{{ asset('/galerias/paginas/impermeabilizacao.jpg') }}" alt="Impermeabilização">
              </div>
              <div class="fbox-content px-0">
                <h3>Impermeabilização</h3>
                <p><b>Impermeabilização:</b> sofás, cadeiras, estofados de carros entre outros, assim evitando manchas no tecido e aumentando a durabilidade. Acesse nossa página e entenda mais sobre a impermeabilização.</p>
                <a href="{{url('/impermeabilizacao')}}" class="button button-small button-dark button-rounded button-pink"><i class="fa fa-gift"></i>Saiba mais</a>
              </div>
            </div>
          </div>

        </div><!-- row -->

      </div><!-- /.container -->
    </section><!-- /.features -->
@endsection