Nome: {{$email->name}} <br>
E-mail: {{$email->email}} <br>
Telefone: <a href="https://api.whatsapp.com/send?phone=55{{$email->phoneMask}}">{{$email->phone}}</a> <br>
Bairro: {{$email->distric}} <br>
Referência: {{$email->referencia}} <br>
Mensagem: {{$email->message}} <br> <br>
Data: {{\Carbon\Carbon::now()->format('d/m/Y H:i')}}