/*--------------------------
    Project Name: Amarou
    Version: 1.0
    Author: 7oorof
    Devloped by: Ahmed Abdallah (a.abdallah999@gmail.com)
    Relase Date: August 2020
---------------------------*/
/*---------------------------
      Table of Contents
    --------------------
    01- Pre Loading
    02- Mobile Menu
    03- Sticky Navbar
    04- Search Popup 
    05- Scroll Top Button
    06-  Scroll Top Button
    07- Set Background-img to section 
    08- Add active class to accordions
    09- Load More Items
    10- Slick Carousel
    11- Popup Video
    12- CounterUp
    13- NiceSelect Plugin
     
 ----------------------------*/

$(function() {

    "use strict";

    /* Global variables*/
    var $win = $(window);


    /*==========   Mobile Menu   ==========*/
    var $navToggler = $('.navbar-toggler');
    $navToggler.on('click', function() {
        $(this).toggleClass('actived');
    })
    $navToggler.on('click', function() {
        $('.navbar-collapse').toggleClass('menu-opened');
    })

    /*==========   Sticky Navbar   ==========*/
    $win.on('scroll', function() {
        if ($win.width() >= 992) {
            var $navbar = $('.navbar');
            if ($win.scrollTop() > 50) {
                $navbar.addClass('is-sticky');
            } else {
                $navbar.removeClass('is-sticky');
            }
        }
    });

    /*==========  Search Popup  ==========*/
    $('.action-btn__search').on('click', function(e) {
        e.preventDefault();
        $('.search-popup').toggleClass('active', 'inActive').removeClass('inActive');
    });
    /* Close Module Search*/
    $('.search-popup__close').on('click', function() {
        $('.search-popup').removeClass('active').addClass('inActive');
    });

    /*==========   Scroll Top Button   ==========*/
    var $scrollTopBtn = $('#scrollTopBtn');
    /* Show Scroll Top Button*/
    $win.on('scroll', function() {
        if ($(this).scrollTop() > 700) {
            $scrollTopBtn.addClass('actived');
        } else {
            $scrollTopBtn.removeClass('actived');
        }
    });
    /* Animate Body after Clicking on Scroll Top Button*/
    $scrollTopBtn.on('click', function() {
        $('html, body').animate({
            scrollTop: 0
        }, 500);
    });

    var $btnWhatsapp = $('.btn-whatsapp');
    /* Show Scroll Top Button*/
    $win.on('scroll', function() {
        if ($(this).scrollTop() > 0) {
            $btnWhatsapp.addClass('actived');
        } else {
            $btnWhatsapp.removeClass('actived');
        }
    });


    /*==========   Set Background-img to section   ==========*/
    $('.bg-img').each(function() {
        var imgSrc = $(this).children('img').attr('src');
        $(this).parent().css({
            'background-image': 'url(' + imgSrc + ')',
            'background-size': 'cover',
            'background-position': 'center',
        });
        $(this).parent().addClass('bg-img');
        if ($(this).hasClass('background-size-auto')) {
            $(this).parent().addClass('background-size-auto');
        }
        $(this).remove();
    });

    /*==========   Add active class to accordions   ==========*/
    $('.accordion__item-header').on('click', function() {
        $(this).parent('.accordion-item').addClass('opened');
        $(this).parent('.accordion-item').siblings().removeClass('opened');
    })
    $('.accordion__item-title').on('click', function(e) {
        e.preventDefault()
    });

    /*==========   Load More Items  ==========*/
    function loadMore(loadMoreBtn, loadedItem) {
        $(loadMoreBtn).on('click', function(e) {
            e.preventDefault();
            $(this).fadeOut();
            $(loadedItem).fadeIn();
        })
    }

    loadMore('.loadMoreportfolio', '.portfolio-hidden > .portfolio-item');


    /*==========  Contact Form validation  ==========*/
    var contactForm = $("#contactForm"),
        contactResult = $('.contact-result');
    contactForm.validate({
        debug: false,
        submitHandler: function(contactForm) {
            $(contactResult, contactForm).html('<div class="alert alert-info" role="alert"><strong>Aguarde. Estamos preparando o envio do seu orçamento.</strong></div>');
            $(contactForm).hide('slow');
            $.ajax({
                type: "POST",
                url: "/orcar",
                data: $(contactForm).serialize(),
                timeout: 20000,
                success: function(msg) {
                    $(contactResult, contactForm).html('<div class="alert alert-success" role="alert"><strong>Obrigado. Recebemos seu orçamento, entraremos em contato.</strong></div>').delay(3000).fadeOut(2000);
                    $('#name, #email, #phone, #distric, #referencia, #message').val('');
                    setTimeout(function() {
                        $(contactForm).show('slow');
                    }, 2050);
                },
                error: $('.thanks').show()
            });
            return false;
        }
    });

    /*==========   Slick Carousel ==========*/
    $('.slick-carousel').slick();

    /*==========  NiceSelect Plugin  ==========*/
    $('select').niceSelect();
});