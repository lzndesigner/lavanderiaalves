<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Front\HomeController@index');

  Route::get('/servicos', 'Front\HomeController@servicos');
  Route::get('/lavagem-de-estofados', 'Front\ServicesController@estofados');
  Route::get('/lavagem-de-tapetes', 'Front\ServicesController@tapetes');
  Route::get('/lavagem-de-cortinas', 'Front\ServicesController@cortinas');
  Route::get('/lavagem-de-persianas', 'Front\ServicesController@persianas');
  Route::get('/lavagem-de-carpetes', 'Front\ServicesController@carpetes');
  Route::get('/impermeabilizacao', 'Front\ServicesController@impermeabilizacao');  
  Route::get('/lavagem-sofa-gif-animado', 'Front\ServicesController@lavagemGif');  

Route::get('/orcamento', 'Front\HomeController@orcamento');
Route::get('/namidia', 'Front\HomeController@namidia');
Route::get('/local', 'Front\HomeController@local');


Route::post('/orcar', 'Front\HomeController@sendmail')->name('orcar.sendmail');
