const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.styles([
    'public/frontend/css/basic.css',
    'public/frontend/css/components.css',
    'public/frontend/css/custom.css',
    'public/frontend/css/libraries.css',
    'public/frontend/css/style.css',

], 'public/minify/css/all.css');