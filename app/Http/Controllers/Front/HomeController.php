<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;
use DB;
use Carbon\Carbon;

class HomeController extends Controller
{
  // Dashboard
  public function index()
  {
    return view('frontend.home');
  }

  // servicos
  public function servicos()
  {
    return view('frontend.servicos');
  }
  
  // orcamento
  public function orcamento()
  {
    return view('frontend.orcamento');
  }
    
  // local
  public function local()
  {
    return view('frontend.local');
  }
  
  // local
  public function namidia()
  {
    return view('frontend.namidia');
  }
  
  public function sendmail(Request $request)
  {
    $remove = ['(', ')', ' ', '-'];
    $insert = ['', '', '', ''];
    $phoneMask = str_replace($remove, $insert, $request->phone);

    $request->request->add(['phoneMask' => $phoneMask]);

    Mail::send(new ContactMail($request));
    return redirect('/');
  }
  
}
