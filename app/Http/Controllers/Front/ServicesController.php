<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;
use DB;
use Carbon\Carbon;

class ServicesController extends Controller
{

  // servicos
  public function estofados()
  {
    return view('frontend.servicos.estofados');
  }
  public function tapetes()
  {
    return view('frontend.servicos.tapetes');
  }
  public function cortinas()
  {
    return view('frontend.servicos.cortinas');
  }
  public function persianas()
  {
    return view('frontend.servicos.persianas');
  }
  public function carpetes()
  {
    return view('frontend.servicos.carpetes');
  }
  public function impermeabilizacao()
  {
    return view('frontend.servicos.impermeabilizacao');
  }
  public function lavagemGif()
  {
    return view('frontend.servicos.lavagemGif');
  }
  
}
